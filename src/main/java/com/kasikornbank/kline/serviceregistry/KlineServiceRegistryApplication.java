package com.kasikornbank.kline.serviceregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author binh_trong.t
 * @created 2562/04/18
 */

@SpringBootApplication
@EnableEurekaServer
public class KlineServiceRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(KlineServiceRegistryApplication.class, args);
	}

}
